CREATE TABLE 
    item_category(
        category_id int AUTO_INCREMENT PRIMARY KEY, 
        category_name varchar(256) NOT NULL);
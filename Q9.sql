SELECT
    category_name,
    SUM(item_price) As total_price
FROM
    item i    
INNER JOIN
    item_category ic
ON
    i.category_id = iC.category_id
GROUP BY
    category_name
ORDER BY
    total_price DESC
;